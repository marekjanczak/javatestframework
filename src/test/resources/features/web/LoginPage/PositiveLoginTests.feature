@Positive
Feature: Positive login tests

  Background:
    Given I navigate to login page
    And I click sign in link

  Scenario: Log in to the application with positive result
    When I log in with correct data
    Then I log in to the application