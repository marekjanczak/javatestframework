@Positive
Feature: Failed login tests

  Background:
    Given I enter store link
    And I click sign in link

  Scenario: Log in to the application with negative result
    When I log in with incorrect data
    Then I see warning message that user name or password is invalid