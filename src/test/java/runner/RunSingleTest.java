package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        features = "src/test/resources",
        tags = {"@Positive"},
        glue = {"steps"},
        monochrome = true,
        dryRun = false)
public class RunSingleTest {
}
