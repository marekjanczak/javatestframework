package configuration;

import org.aeonbits.owner.ConfigFactory;

public class PropertiesLoader {

    public static Configuration getProperty() {
        return ConfigFactory.create(Configuration.class);
    }
}
