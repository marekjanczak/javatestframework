package configuration;

import driver.BrowserType;

public class TestRunProperties {

    public static BrowserType getBrowserToRun() {
        return BrowserType.valueOf(PropertiesLoader.getProperty().browser());
    }

    public static boolean getIsRemoteRun(){
//        return Boolean.parseBoolean(ConfigurationProperties.getProperties().getProperty("is.remote.run"));
        return PropertiesLoader.getProperty().isRemoteRun();
    }

    public static String getGridUrl() {
//        return ConfigurationProperties.getProperties().getProperty("grid.url");
        return PropertiesLoader.getProperty().gridURL();
    }
}
