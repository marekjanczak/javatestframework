package configuration;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources("classpath:configuration.properties")
public interface Configuration extends Config {

    @Key("is.remote.run")
    boolean isRemoteRun();

    @Key("grid.url")
    String gridURL();

    @Key("app.url")
    String appURL();

    @Key("browser")
    String browser();
}
