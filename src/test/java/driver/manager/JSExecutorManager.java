package driver.manager;

import org.openqa.selenium.JavascriptExecutor;

import static driver.manager.DriverManager.getWebDriver;

public class JSExecutorManager {

    private static JavascriptExecutor executor = null;

    public static JavascriptExecutor getJSExecutor() {

        if (executor == null) {
            executor = (JavascriptExecutor) getWebDriver();
        }

        return executor;
    }

    public static void disposeJSExecutor() {
        executor = null;
    }
}
