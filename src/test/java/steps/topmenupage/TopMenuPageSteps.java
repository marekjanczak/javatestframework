package steps.topmenupage;

import cucumber.api.java.en.Given;
import page.objects.TopMenuPage;

public class TopMenuPageSteps {

    private TopMenuPage topMenuPage = new TopMenuPage();

    @Given("^I click sign in link$")
    public void i_click_sign_in_link() {
        topMenuPage.clickOnSignInLink();
    }
}
