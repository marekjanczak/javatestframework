package steps.footerpage;

import cucumber.api.java.en.Then;
import page.objects.FooterPage;

import static org.junit.Assert.assertTrue;

public class FooterPageSteps {

    private FooterPage footerPage = new FooterPage();

    @Then("^I log in to the application$")
    public void iLogInToTheApplication() {
        assertTrue(footerPage.isBannerAfterLoginDisplayed());
    }
}
