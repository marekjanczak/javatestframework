package steps.loginpage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.manager.DriverUtils;
import navigation.ApplicationURLs;
import page.objects.LandingPage;
import page.objects.LoginPage;
import page.objects.TopMenuPage;

import static org.junit.Assert.assertEquals;

public class LoginPageSteps {

    private LandingPage landingPage = new LandingPage();
    private LoginPage loginPage = new LoginPage();

    @Given("^I enter store link$")
    public void i_enter_store_link() {
        landingPage.clickOnEnterStoreLink();
    }

    @When("^I log in with incorrect data$")
    public void i_log_in_with_incorrect_data() {
        loginPage.typeIntoUserNameField("NotExistingLogin");
        loginPage.typeIntoPasswordField("NotProperPassword");
        loginPage.clickOnLoginButton();
    }

    @Then("^I see warning message that user name or password is invalid$")
    public void i_see_warning_message_that_user_name_or_password_is_invalid() {
        assertEquals(loginPage.getWarningMessage(), "Invalid username or password. Signon failed.");
    }

    @When("^I log in with correct data$")
    public void iLogInWithCorrectData() {
        loginPage.typeIntoUserNameField("j2ee");
        loginPage.typeIntoPasswordField("j2ee");
        loginPage.clickOnLoginButton();
    }

    @Given("^I navigate to login page$")
    public void iNavigateToLoginPage() {
        DriverUtils.navigateToPage(ApplicationURLs.LOGIN_URL);
    }
}
