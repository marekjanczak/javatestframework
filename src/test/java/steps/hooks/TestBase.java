package steps.hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import driver.manager.DriverManager;
import driver.manager.DriverUtils;
import io.qameta.allure.Step;
import utils.ScreenShotMaker;

import static navigation.ApplicationURLs.APPLICATION_URL;

public class TestBase {

    @Step("Setting up browser to and navigating to Home Page")
    @Before(order = 1)
    public void beforeTest() {
        DriverManager.getWebDriver();
        DriverUtils.setInitialConfiguration();
        DriverUtils.navigateToPage(APPLICATION_URL);
    }

    @After(order = 1)
    public void embedScreenshot(Scenario scenario) {
        if(scenario.isFailed()) {
            try {
                scenario.embed(ScreenShotMaker.makeScreenShot(), "image/png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Step("Disposing browser")
    @After(order = 0)
    public void afterTest() {
        DriverManager.disposeDriver();
    }
}
