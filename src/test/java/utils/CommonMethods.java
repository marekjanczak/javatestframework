package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static driver.manager.DriverManager.getWebDriver;
import static waits.JSWaiter.waitForElementsAreComplete;
import static waits.JSWaiter.waitForPageLoadComplete;
import static waits.WaitForElement.waitAndClick;

public class CommonMethods {

    private static Logger log = LogManager.getLogger(Action.class.getName());

    public static void clickOnDisplayedElementFromTheList(List<WebElement> elements, String messageIfNoDisplayed) {
        waitForPageLoadComplete();

        waitAndClick(elements
                .stream()
                .filter(WebElement::isDisplayed)
                .findFirst()
                .orElseThrow(() -> new NotFoundException(messageIfNoDisplayed)));
    }

    public static void clickOnGivenElementFromTheList(List<WebElement> elements, String value, String errorMessage) {
        waitForElementsAreComplete(elements, 5);

        waitAndClick(elements.stream()
                .filter(catalogVersion -> catalogVersion.getText().equals(value))
                .findFirst()
                .orElseThrow(() -> new NotFoundException(errorMessage)));
    }

    public static void selectCorrectOption(WebElement webElement, String option) {
        new Select(webElement).selectByVisibleText(option);
    }

    public static void refreshThePage() {
        getWebDriver().navigate().refresh();
    }

    public static void clearAndSendKeys(WebElement element, String text) {
        try {
            element.clear();
            element.sendKeys(text);
        } catch (StaleElementReferenceException e) {
            log.error("StaleElementReferenceException for " + element);
        }
    }
}
