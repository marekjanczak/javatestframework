package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.event.InputEvent;
import java.time.Duration;

import static driver.manager.DriverManager.getWebDriver;
import static waits.WaitForElement.simpleWaiter;

public class Action {

    private static Logger log = LogManager.getLogger(Action.class.getName());

    private Actions action = new Actions(getWebDriver());

    public void doubleClick(WebElement element) {

        boolean isClicked = false;
        int attempts = 0;
        while (!isClicked && attempts < 8) {
            try {
                action.doubleClick(element).perform();
                isClicked = true;
            } catch (StaleElementReferenceException e) {
                attempts++;
            }
        }

        if (!isClicked) {
            try {
                throw new Exception("Could not click " + element + ", after 5 attempts");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void moveToElement(WebElement element) {
        action.moveToElement(element);
    }
}

