package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReaders {

    public static String readFileAsString(String fileName) {
        String text = "";
        try {
            text = text + new String(Files.readAllBytes(Paths.get("src/main/resources/" + fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }


}
