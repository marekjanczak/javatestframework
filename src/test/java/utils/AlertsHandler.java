package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.Alert;

public class AlertsHandler {

    private static Alert getWindowAlert() {
        return DriverManager.getWebDriver().switchTo().alert();
    }

    public static String getTextFromAlertMessage() {
        String text = getWindowAlert().getText();
        closeWindowAlert();
        return text;
    }

    private static void closeWindowAlert() {
        getWindowAlert().accept();
    }
}
