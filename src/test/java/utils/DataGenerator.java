package utils;

import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.*;

public class DataGenerator {

    private static Random random = new Random();

    public static String getRandomData(List<String> data) {
        return data.get(random.nextInt(data.size() - 1));
    }

    public static int getRandomNumber(int minimumNumber, int maximumNumber) {
        return minimumNumber + (int) (Math.random() * ((maximumNumber - minimumNumber) + 1));
    }

    public static String generateStringWithNumericCharacters(int length) {
        return randomNumeric(length);
    }

    public static String generateStringWithAlphabeticCharacters(int length) {
        return randomAlphabetic(length);
    }

    public static String generateStringWithMixedCharacters(int length) {
        return random(length, true, true);
    }

    public static int getRandomIndex(List<WebElement> options) {
        return random.nextInt(options.size() - 1);
    }
}
