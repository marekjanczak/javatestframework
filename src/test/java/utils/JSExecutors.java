package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class JSExecutors {

    private static JavascriptExecutor executor = (JavascriptExecutor) DriverManager.getWebDriver();

    public static void loadAllJSScriptsOnPage() {

        boolean isAllScriptsOnPageDownloaded = true;

        do {
            if (executor.executeScript("return document.readyState").toString().equals("complete")) {
                isAllScriptsOnPageDownloaded = false;
            }
        } while (isAllScriptsOnPageDownloaded);
    }

    public static void jsExecutorWithElement(WebElement element, String script) {
        executor.executeScript(script, element);
    }

    public static void jsExecutorWithoutElement(String script) {
        executor.executeScript(script);
    }

}
