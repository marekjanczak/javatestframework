package page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static waits.WaitForElement.*;

public class LandingPage extends BasePage {

    @FindBy(css = "#Content a")
    private WebElement enterStoreLink;

    @Step("Click on Enter Store link")
    public void clickOnEnterStoreLink() {
        waitUntilElementIsClickable(enterStoreLink);
        enterStoreLink.click();

        log().info("Clicked on Enter Store link");
    }
}
