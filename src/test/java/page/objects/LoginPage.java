package page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static waits.WaitForElement.*;

public class LoginPage extends BasePage {

    @FindBy(name = "username")
    private WebElement usernameField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(name = "signon")
    private WebElement signOnButton;

    @FindBy(css = "#Content ul[class='messages'] li")
    private WebElement messageLabel;

    @Step("Type into User Name Field {userName}")
    public void typeIntoUserNameField(String userName) {
        waitUntilElementIsVisible(usernameField);
        usernameField.clear();
        usernameField.sendKeys(userName);

        log().info("Typed into User Name Field {}", userName);
    }

    @Step("Type into Password Field {password}")
    public void typeIntoPasswordField(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);

        log().info("Typed into Password Field {}", password);
    }

    @Step("Click on Login Button")
    public void clickOnLoginButton() {
        signOnButton.click();

        log().info("Clicked on Login Button");
    }

    @Step("Getting warning message from Login Page")
    public String getWarningMessage() {
        waitUntilElementIsVisible(messageLabel);
        String warningText = messageLabel.getText();
        log().info("Returned warning message was: {}", warningText);
        return warningText;
    }

}