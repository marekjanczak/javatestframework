package page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static generic.assertions.AssertWebElement.assertThat;
import static waits.WaitForElement.*;

public class FooterPage extends BasePage {

    @FindBy(css = "#Banner img[src*='dog']")
    private WebElement bannerAfterLoginLogo;

    @Step("Getting is dog banner is displayed")
    public boolean isBannerAfterLoginDisplayed() {
        log().info("Checking if dog banner is displayed");
        waitUntilElementIsVisible(bannerAfterLoginLogo);
        assertThat(bannerAfterLoginLogo).isDisplayed();
        return bannerAfterLoginLogo.isDisplayed();
    }

}