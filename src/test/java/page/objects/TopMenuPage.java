package page.objects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static waits.WaitForElement.*;


public class TopMenuPage extends BasePage {

    @FindBy(css = "#MenuContent a[href*='signonForm']")
    private WebElement signOnLink;

    @Step("Click on Sign In Link")
    public void clickOnSignInLink() {
        waitUntilElementIsClickable(signOnLink);
        signOnLink.click();

        log().info("Clicked on Sign on Link");
    }
}

