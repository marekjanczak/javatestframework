package waits;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static driver.manager.DriverManager.getWebDriver;
import static driver.manager.JSExecutorManager.getJSExecutor;
import static waits.WaitForElement.simpleWaiter;

public class JSWaiter {

    private static WebDriverWait getWebDriverWait() {
        return new WebDriverWait(getWebDriver(), 10);
    }

    private static void ajaxComplete() {
        getJSExecutor().executeScript("var callback = arguments[arguments.length - 1];"
                + "var xhr = new XMLHttpRequest();" + "xhr.open('GET', '/Ajax_call', true);"
                + "xhr.onreadystatechange = function() {" + "  if (xhr.readyState == 4) {"
                + "    callback(xhr.responseText);" + "  }" + "};" + "xhr.send();");
    }

    private static void waitForJQueryLoad() {
        try {
            ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) getWebDriver())
                    .executeScript("return jQuery.active") == 0);

            boolean jqueryReady = (Boolean) getJSExecutor().executeScript("return jQuery.active==0");

            if (!jqueryReady) {
                getWebDriverWait().until(jQueryLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }

    private static void waitForAngularLoad() {
        String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";
        angularLoads(angularReadyScript);
    }

    private static void waitUntilJSReady() {
        try {
            ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) getWebDriver())
                    .executeScript("return document.readyState").toString().equals("complete");

            boolean jsReady = getJSExecutor().executeScript("return document.readyState").toString().equals("complete");

            if (!jsReady) {
                getWebDriverWait().until(jsLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }

    private static void waitUntilJQueryReady() {
        Boolean jQueryDefined = (Boolean) getJSExecutor().executeScript("return typeof jQuery != 'undefined'");
        if (jQueryDefined) {
            simpleWaiter(20);

            waitForJQueryLoad();

            simpleWaiter(20);
        }
    }

    private static void waitUntilAngularReady() {
        try {
            Boolean angularUnDefined = (Boolean) getJSExecutor().executeScript("return window.angular === undefined");
            if (!angularUnDefined) {
                Boolean angularInjectorUnDefined = (Boolean) getJSExecutor().executeScript("return angular.element(document).injector() === undefined");
                if (!angularInjectorUnDefined) {
                    simpleWaiter(20);

                    waitForAngularLoad();

                    simpleWaiter(20);
                }
            }
        } catch (WebDriverException ignored) {
        }
    }

    private static void waitUntilAngular5Ready() {
        try {
            Object angular5Check = getJSExecutor().executeScript("return getAllAngularRootElements()[0].attributes['ng-version']");
            if (angular5Check != null) {
                Boolean angularPageLoaded = (Boolean) getJSExecutor().executeScript("return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1");
                if (!angularPageLoaded) {
                    simpleWaiter(20);

                    waitForAngular5Load();

                    simpleWaiter(20);
                }
            }
        } catch (WebDriverException ignored) {
        }
    }

    private static void waitForAngular5Load() {
        String angularReadyScript = "return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1";
        angularLoads(angularReadyScript);
    }

    private static void angularLoads(String angularReadyScript) {
        try {
            ExpectedCondition<Boolean> angularLoad = driver -> Boolean.valueOf(((JavascriptExecutor) driver)
                    .executeScript(angularReadyScript).toString());

            boolean angularReady = Boolean.valueOf(getJSExecutor().executeScript(angularReadyScript).toString());

            if (!angularReady) {
                getWebDriverWait().until(angularLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }

    public static void waitForPageLoadComplete() {
        waitUntilJSReady();
        ajaxComplete();
        waitUntilJQueryReady();
        waitUntilAngularReady();
        waitUntilAngular5Ready();
    }

    /**
     * Method to make sure a specific element has loaded on the page
     *
     * @params elements
     * @param expected
     */
    public static void waitForElementsAreComplete(List<WebElement> elements, int expected) {
        ExpectedCondition<Boolean> angularLoad = driver -> {
            int loadingElements = elements.size();
            return loadingElements >= expected;
        };
        getWebDriverWait().until(angularLoad);
    }

    public static void waitForElementIsComplete(WebElement element) {
        ExpectedCondition<Boolean> angularLoad = driver ->
                element.isDisplayed();
        getWebDriverWait().until(angularLoad);
    }

    /**
     * Waits for the elements animation to be completed
     *
     * @params elements
     */
    public static void waitForAnimationToComplete(List<WebElement> elements) {
        ExpectedCondition<Boolean> angularLoad = driver -> {
            int loadingElements = elements.size();
            return loadingElements == 0;
        };
        getWebDriverWait().until(angularLoad);
    }
}
