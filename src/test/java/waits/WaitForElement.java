package waits;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import waiter.Waiter;

import java.util.List;

import static driver.manager.DriverManager.getWebDriver;

public class WaitForElement {

    private static Logger log = LogManager.getLogger(WaitForElement.class.getName());

    private static Waiter waiter = new Waiter();

    private static WebDriverWait getWebDriverWait() {
        return new WebDriverWait(DriverManager.getWebDriver(), 10);
    }

    public static void waitUntilElementIsVisible(WebElement element) {
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilElementIsClickable(WebElement element) {
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitUntilElementToBeClicable(WebElement element) {
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitUntilAllElementsAreVisible(List<WebElement> elements) {
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public static void clickElementAndWaitForPageLoadComplete(WebElement element) {
        waiter.clickElementAndWaitForPageLoadComplete(element, getWebDriver());
    }

    public static void waitAndClick(WebElement element) {
        try {
            waiter.click(element, getWebDriver(), 30);
        } catch (StaleElementReferenceException e) {
            log.error("StaleElementReferenceException for " + element);
        }

    }

    public static void simpleWaiter(int miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
