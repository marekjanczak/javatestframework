# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Testy napisane w języku java, selenium, cucumber.
To reposytorium jest po to aby pokazać parę innowacji (nowych bibliotek) w testach.

### New things in tests ###

1. Biblioteka, dzięki której nie musimy zaciągać statycznie driver'ów dla poszczególnych przeglądarek. Korzystamy z wersji przegladarek zainstalowaych na maszynie. 

<dependency>
  <groupId>io.github.bonigarcia</groupId>
  <artifactId>webdrivermanager</artifactId>
  <version>3.3.0</version>
  <scope>test</scope>
</dependency>

Wiecej informacji na: https://github.com/bonigarcia/webdrivermanager

2. Biblioteka do zczytywania danych z plików .properties. Łatwa w użyciu, szybka i elastyczna.

<dependency>
   <groupId>org.aeonbits.owner</groupId>
   <artifactId>owner-java8</artifactId>
   <version>1.0.6</version>
</dependency>

Wiecej informacji na: http://owner.aeonbits.org/

3. Stworzenie w driver/listeners nowej klasy DriverEventListeners, która ma za zadanie pokazanie jak najwięcej zdarzeń w trakcie wykonania testu

4. Biblioteka do tworzenia randomowych danych. Nie trzeba tworzyć własnych skryptów. 

org.apache.commons.lang3.RandomStringUtils.*

Wiecej informacji na: http://commons.apache.org/proper/commons-lang/javadocs/api-3.9/org/apache/commons/lang3/RandomStringUtils.html

5. Stworzenie w package waits nowej klasy, JSWaiter, która jest przydatna przy testach np. hybris backoffice. Zwykłe waitery czasami nie działają a ta klasa będzie pomocna. 
Zawiera wszystkie js executory, które obsługują angular, ajax, jquery.
